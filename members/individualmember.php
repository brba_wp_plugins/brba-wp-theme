<?php

global $wpdb;
global $member_table_name;

$id = $_GET["id"];

$member = $wpdb->get_row("SELECT * from $member_table_name WHERE id=$id");
$name = "$member->first_name $member->last_name"

?>

<div id="main-members-content">
    <title> Members Page </title>
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/members/main.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/members/individualmember.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
            integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

        <div id="individualContainer">
            <div id="individualLeft">
                <div class="individualTags">
                    <span class="tag">Board of Directors</span>
                    <span class="tag">Business Services</span>
                </div>
                <div class="individualImg">
                    <img class="largeCirc" src="<?php echo get_bloginfo('template_directory') ?>/members/logo.jpg">
                </div>
                <div class="individualContact">
                    <h3 class="individualContactTop">Contact <?=$member->company_name?></h3>
                    <p class="individualContactBot"><?=$member->email_public?> <?=$member->phone_number?>
                        <i class="fab fa-instagram"></i>
                        <i class="fab fa-twitter"></i>
                        <i class="fab fa-facebook-f"></i>
                    </p>
                </div>
            </div>
            <div id="individualRight">
                <h1 class="individualTitle"><?=$member->company_name?></h1>
                <div class="individualInfo">
                    <h3><strong>Website: </strong><a href="<?=$member->website?>" target="_blank"><?=$member->website?></a></h3>
                    <h3><strong>Name: </strong><?=$name?></h3>
                    <h3><strong>Address: </strong><?=$member->address?></h3>
                </div>
                <div class="individualMap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d24189.303276039904!2d-74.6123729858783!3d40.72543662713936!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c39800c724488d%3A0x3d4eef5230ebcd86!2sBernardsville%2C+NJ!5e0!3m2!1sen!2sus!4v1561144877912!5m2!1sen!2sus" width="100%" height="450px" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
</div>
