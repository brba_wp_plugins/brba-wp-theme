<?php

$admin_mail = get_option('admin_email');

$phpmailer->addAddress($admin_mail);

if (isset($_POST["form"])) {

    if ($_POST["form"] == "true") {
        $data = $_POST;

        // process data
        unset($data["form"]);
        $data["validated"] = false;

        foreach ($_POST as $key => $value) {
            $_POST[$key] = sanitize_text_field($value);
        }

        // Insert our data into the database
        global $wpdb;
        global $member_table_name;

        $wpdb->insert(
            $member_table_name,
            $data
        );

        // send email

        wp_mail($admin_mail, "New member application", "A new member application has been submitted. Please visit the site to review it.");



    }

}



?>


<!DOCTYPE html>
<html style="background-color:#69B578;">

<head>
    <title>Member Application</title>
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory') ?>/members/member_form/member_form.css">

    <script>
        window.console = window.console || function (t) { };
    </script>

</head>

<body translate="no">

    <div class="RSVP-page" style="margin-top: 3%;">
    <form method="POST">
        <center>
            <h1>Member Application</h1>
            <button id="return-to-events" onclick="window.location.href = '/members';" class="button-med-green">
                Return to Member List
            </button>
        </center>

        <input type="hidden" name="form" value="true" />


        <div class="question">
            <input type="text" name="first_name" required />
            <label>First Name</label>
        </div>
        <div class="question">
            <input type="text" name="last_name" required />
            <label>Last Name</label>
        </div>
        <div class="question">
            <input type="text" name="business_title" required />
            <label>Business Title</label>
        </div>
         <div class="question">
            <input type="text" name="company_name" required />
            <label>Company Name</label>
        </div>

        <div class="question">
            <input type="text" name="email_brba" required />
            <label>Email for BRBA to use</label>
        </div>

        <div class="question">
            <input type="text" name="email_public" required />
            <label>Email displayed on site</label>
        </div>

        <div class="question">
            <input type="text" name="phone_number" required />
            <label>Office Phone</label>
        </div>

        <div class="question">
            <input type="text" name="website" required />
            <label>Web Address</label>
        </div>

        <div class="question">
            <input type="text" name="web_presence" required />
            <label> Second Web Address (FB, LinkedIn, Blog , etc.)</label>
        </div>


        <div class="question">
            <input type="text" name="company_description" required />
            <label>Company Description(500 characters max)</label>
        </div>



        <div class="question">
            <input type="text" name="primary_business_needs" required />
            <label>What are your primary business needs?</label>
        </div>


        <div class="question">
            <input type="text" name="membership_takeaway" required />
            <label>What would you like to get from your membership?</label>
        </div>


        <div class="question">
            <input type="text" name="brba_hear_about" required />
            <label>How did you hear about BRBA?</label>
        </div>

        <div class="question">
            <input type="text" name="address" required />
            <label>Address</label>
        </div>

        <div class="question">
            <input type="text" name="city" required />
            <label>City</label>
        </div>

        <div class="question">
            <input type="text" name="state" required />
            <label>State</label>
        </div>

        <div class="question">
            <input type="text" name="zip_code" required />
            <label>ZIP Code</label>
        </div>







        <div class="question">
            <div class ="attend">
                <label>Would you like the address to be public?</label>
                <input type="checkbox" name="address_public">
            </div>
        </div>


        <div class="question">

            <div class ="attend">
                <label>Photo (recommended size 100 pixels wide)</label>
                <input type="file" name="photo" accept="image/*">
            </div>


            <div class="question">
                <div class ="attend">
                    <label> Logo (recommended size 300 pixels wide)</label>
                    <input type="file" name="logo" accept="image/*">
                </div>
            </div>

            <div class="RSVP-form-button">
                <button id="submit" input ="submit" class="button-med-green">Submit</button>
            </div>

        </div>



    </form>

</div>

</body>

</html>
