<!DOCTYPE html>
<html>
    <head>
    	  <title> Members Page </title>
        <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/members/main.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/members/members.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
              integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    </head>
    <body>
        <div style="margin-bottom: 3%;">

        <div id="searchBar">
            <form method="GET">

                <input type="text" placeholder="Find a member" id="searchBarInput" name="search">
                <input type="submit" value="Search" class="button-med-green">
                <button class="button-med-green" >Clear</button>

            </form>

            <a href="/members">

            </a>

        </div>

        <div style="">
                <button class="button-med-green" id="member-apply" onclick="location.replace('/member-application')">Apply to be a Member</button>
    


        </div>

        </div>


        <?php

        global $wpdb;
        global $member_table_name;

        $members = $wpdb->get_results("SELECT * from $member_table_name WHERE validated=1;");

        if (isset($_GET["search"])) {

            $term = $_GET["search"];

            $query = "SELECT * FROM $member_table_name WHERE validated=1 AND company_name LIKE '%$term%' OR categories LIKE '%$term%'";

            $members = $wpdb->get_results($query);

        }

        foreach ($members as $member) {

            $categories_string = $member->categories;
            $categories = explode(",", $categories_string);

            if ($categories[0] == "") {

                $categories = array();

            }

        ?>
            <div class="memberProfile">
                <div class="memberImg">
                    <img class="smallCirc" src="<?php echo get_bloginfo('template_directory') ?>/members/logo.jpg">
                </div>
                <div class="memberTitle">
                    <div><h2 class="memberTitleText" onclick="window.location.href='/member-view?id=<?=$member->id?>'"><?php echo $member->company_name ?></h2></div>
                </div>
                <div class="memberTags">

                    <?php

                    foreach ($categories as $category):

                    ?>

                    <span class="tag"><?=$category?></span>

                    <?php endforeach; ?>
                </div>
            </div>

<?php
        }



        ?>


    </body>
</html>
