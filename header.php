<!doctype html>

<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="<?php echo get_bloginfo( 'template_directory' );?>/theme.css" rel="stylesheet">
        <link href="<?php echo get_bloginfo( 'template_directory' );?>/style.css" rel="stylesheet">
        <link href="<?php echo get_bloginfo( 'template_directory' );?>/home.css" rel="stylesheet">

        <style>
         #wpadminbar { display:none !important;}

        </style>

        <?php wp_head() ?>
    </head>

    <body>

        <div>


        <div id="TabBar" class="blog-masthead">
            <ul>
                <li><img id="logo" src="https://www.ridgebusiness.org/Assets/website%20logo.png"></li>
                <li><img id="login" src="https://i.pinimg.com/originals/dc/f6/b7/dcf6b71f6d4fbb0a0f541a90e67f852c.png"></li>
                <li class="wordItem">
                    <a href="/">HOME</a>
                </li>
                <li class="wordItem">
                    <a href="/members">MEMBERS</a>
                </li>
                <li class="wordItem">
                    <a href="/bulletin">BULLETIN</a>
                </li>
                <li class="wordItem">
                    <a href="/events">EVENTS</a>
                </li>
                <li class="wordItem">
                    <a href="/jobs">JOBS</a>
                </li>
                <li class="wordItem">
                    <a href="/login">LOGIN</a>
                </li>

            </ul>
        </div>


