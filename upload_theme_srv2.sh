#!/bin/bash

zip -r brba-wp-theme.zip *
scp brba-wp-theme.zip brba-demo.now.im:~/
ssh brba-demo.now.im "sudo -u www-data wp --path=/var/www/wordpress theme install --force ~/brba-wp-theme.zip"
rm  brba-wp-theme.zip
