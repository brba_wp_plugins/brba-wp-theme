<!DOCTYPE html>
<html>
    <head>
        <title>BRBA - Home</title>
        <!-- Links stylesheets from the stylesheet, fonts, animation libraries -->
        <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/main.css">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <link rel="stylesheet" type="text/css" href="../Tags/tag.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">
        <meta charset="utf-8">
    </head>
    <body>
        <div class="grid-container" style="margin-top: 3%">
            <div class="main_page_Text">
                <h1> Basking Ridge Business Alliance </h1>

                <p> The Basking Ridge Business Alliance is a non-profit organization of local business people and residents.

                    Our vision is to successfully promote business prosperity and a healthy community in Basking Ridge. Our mission
                    is to focus on helping businesses and the community work together, nurturing positive growth and interaction, and to
                    preserve those unique qualities that make Basking Ridge a special place in which to live and work.

                </p>

            </div>

            <div class="main_page_pic">
                
                <img class="home_image" src="http://www.ridgebusiness.org/Assets/membership/coldwell_banker_bldg.jpg">
                

            </div>

            <div class="contact_info">
    	          <div class="event-card-changed">
                    
                    <div class="events-content-left">
                        <i class="fab fa-instagram fa-9x"></i>
                    </div>


                    <div class="events-content-centre">
                        <i class="fab fa-facebook fa-9x"></i>
                    </div>


                    <div class="events-content-right">
                        <i class="fab fa-twitter fa-9x"></i>
                    </div>
                </div>

                <h3> 5 Lyons Mall #325, Basking Ridge NJ 07920
			              info@ridgebusiness.org
                </h3>

            </div>
            

            
            <div class="upcome">
    		        <h2> Upcoming Events </h2>


                <?php

                global $member_events_table_name;
                global $wpdb;

                $events = $wpdb->get_results("SELECT * FROM $member_events_table_name");
                $noevents = 0;

                // now filter by 30 days in future
                foreach ($events as $index=>$event) {
                    $time = new DateTime($event->time);
                    $now = new DateTime();



                    $diff = $time->diff($now);

                    if ($diff->days <= 30) {

                ?>
                    <div class="upcoming-events-card">

                        <div class="upcoming1-content-left">
                            <p><?=$event->event_name?>
                        </div>

                        <div class="upcoming1-content-right">
                            <p id="desc1"> <?=$time->format("F d h:i A")?>
                        </div>

                    </div>

                        <?php 

                        }

                        else {

                            $noevents += 1;

                        }


                        }

                        if ($noevents == sizeof($events)) {
                            echo "<p>No events happening soon!</p>";

                        }





                ?>




        </div>
    </body>
</html>
