<!DOCTYPE html>
<html>

<head>
    <title>General Events</title>
    <!-- Links stylesheets from the stylesheet, fonts, animation libraries -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/events/main.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/events/stylesheet.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <br><br>
    <div class="grid-container" style="height: 0 !important;">
        <div class="header">
            <h1 id="title">Events</h1>

        </div>
        <?php

        global $wpdb;
        global $member_events_table_name;

        $evs = $wpdb->get_results("SELECT * from $member_events_table_name");

        $i = 1;

        ?>

        <?php foreach ($evs as $ev): ?>







            <div class="events-card-<?=$i?>">
                <div class="event-card-changed">
                    <div class="events-content-left">
                        <img class="logo" src="https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.iconsdb.com%2Ficons%2Fdownload%2Fgray%2Fcircle-512.png&f=1">
                    </div>
                    <div class="events-content-centre">
                        <h2 id="event-name1"><b> <?=$ev->event_name ?> </b></h2>
                        <p id="date-and-time1"><?=$ev->time ?></p>
                        <p id="location1"> <?=$ev->location ?> </p>
                    </div>
                    <div class="events-content-right">
                        <p id="desc1"> <?php echo $ev->description ?> <a href="/member-event-detail?id=<?=$ev->id?>">Read more </a> </p>
                    </div>
                </div>
            </div>


        <?php $i++; endforeach; ?>


    </div>
</body>

</html>
