<!DOCTYPE html>
<html>

<head>

    <title>Specific Events Page</title>

    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/events/main.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/events/stylesheet.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

</head>

<body>

    <?php

    global $wpdb;
    global $member_events_table_name;

    $id = $_GET["id"];

    $event = $wpdb->get_row("SELECT * from $member_events_table_name WHERE id=$id");
    $date_time = new \DateTime($event->time);








    ?>

    <div class="content">

        <div class="Event-name">
            <h1 id="specific-event-name"><?=$event->event_name?></h1>
        </div>
        <div class="return-button">
            <a href="/events">
                <button id="Return-to-Events-Page" class="button-med-green">Return to Events Page</button></a>
        </div>
        <div class="Event">
            <div id="event-information">

                <div class="image">
                    <img class="logo" src="https://bit.ly/2VBOkZl">
                </div>



                <div class="info-text">

                    <h2 id="event-info">Event Information</h2>


                    <p id="event-time"> Event Time: <?=$date_time->format("g:i a")?>
                    </p>

                    <p id="event-date"> Date: <?=$date_time->format("M d, Y")?></p>


                    <p id="event-desc-long">
                        <?=$event->description?>
                    </p>

                    <a id="rsvp-link" href="/event-rsvp?event_id=<?=$id?>">RSVP</a>
                </div>
            </div>
        </div>

        <?php

        global $member_table_name;

        $member = $wpdb->get_row("SELECT * FROM $member_table_name WHERE id=$event->member_id");
        $name = "$member->first_name $member->last_name";




        ?>



        <div class="contact">

            <h2 id="contact-info">Contact</h2>
            <p id="contact-info-specifics"> Name: <?=$name?> </p>
            <p id="contact-info-specifics"> <i class="fas fa-envelope-square"> </i> <?=$member->email_public?> </p>
            <!--

            <a href="RSVP-form.html">
                <button id="RSVP" class="button-med-green">RSVP</button>
            </a>

            -->



        </div>


        <div class="four">

            <h3 id="event-location">Location: <?=$event->location?></h3>

            <div id="map"></div>
            <script>
                // Initialize and add the map
                function initMap() {
                    // The location of Uluru
                    var uluru = { lat: 40.694054, lng: -74.546970 };
                    // The map, centered at Uluru
                    var map = new google.maps.Map(
                        document.getElementById('map'), { zoom: 14, center: uluru });
                    // The marker, positioned at Uluru
                    var marker = new google.maps.Marker({ position: uluru, map: map });
                }

            </script>
            <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVyfYYjHfALs1EEtI0ZG3i968BGlc5LzQ&callback=initMap">
                </script>
        </div>
</body>

</html>
