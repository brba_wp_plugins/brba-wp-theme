<?php

global $wpdb;
global $member_events_table_name;

$id = $_GET["event_id"];

$event = $wpdb->get_row("SELECT * FROM $member_events_table_name WHERE id=$id");


if ($_POST["form"] == "true") {

    $name = sanitize_text_field($_POST["name"]);
    $email = sanitize_text_field($_POST["email"]);
    $phone = sanitize_text_field($_POST["phone"]);
    $attending = $_POST["attending"];

    if (isset($attending)) {

        $attending = true;

    }
    else {

        $attending = false;

    }




    global $member_rsvp_table;
    global $wpdb;

    $wpdb->insert(
        $member_rsvp_table,
        array(
            "event_id" => $id,
            "name" => $name,
            "email" => $email,
            "phone" => $phone,
            "attending" => $attending
        )
    );




}


?>

<!DOCTYPE html>
<html style="background-color:#69B578;">

<head>
    <title>RSVP Form for <?=$event->event_name?></title>
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory') ?>/events/rsvp/main.css">

    <script>
        window.console = window.console || function (t) { };
    </script>

</head>

<body translate="no">

    <div class="RSVP-page" style="margin-top: 100px;">
        <form method="POST">

            <input type="hidden" name="form" value="true">

        <center>
            <h1>RSVP to <?=$event->event_name?></h1>
            <button id="return-to-events" onclick="window.location.href = '/events';" class="button-med-green">
                Return to Event Information
            </button>
        </center>

        <div class="question">
            <input type="text" required name="name" />
            <label>Name</label>
        </div>
        <div class="question">
            <input type="text" required name="email" />
            <label>Email Address</label>
        </div>
        <div class="question">
            <input type="text" required name="phone" />
            <label>Phone Number</label>
        </div>

        <div class="question">
                <div class ="attend">
                    <label>Are you attending the event?</label>
                    <input type="checkbox" name="attending" class="radio-button" style="display: inline;">
                </div>
            </div>





        <div class="RSVP-form-button">
                <a href="specific-events.html">
            <button id="submit" input ="submit" class="button-med-green">
                Submit
            </button>
        </div>




    </form>

</div>

</body>

</html>
