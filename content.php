<div class="blog-post">
    <h2 class="blog-post-title"><?php the_title(); ?></h2>
    <p class="blog-post-meta"<?php the_date(); ?> by <?php the_author() ?>

     <?php the_content() ?>
</div>
