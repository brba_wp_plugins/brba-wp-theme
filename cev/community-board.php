<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Community Board</title>
        <link rel="stylesheet" href="../../Components/TabBar/TabBar.css">
        <link rel="stylesheet" href="../../Components/Buttons/button.css">

        <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory') ?>/cev/main.css">

        <link rel="stylesheet"  type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans">
        <link rel="stylesheet"  type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/cev/community-board.css">

        <script src="https://code.jquery.com/jquery-3.4.0.min.js"
            integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
            crossorigin="anonymous">
        </script>
        <script
          src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
          integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
          crossorigin="anonymous">
        </script>
        <script src="<?php echo get_bloginfo('template_directory') ?>/cev/community-board.js"></script>
    </head>



    <body>
        <div id="pretty-background-mobile"></div>
        <div id="pretty-background">

        <div id = "grid-container">
        <div class="header">
            <h1 id="title">Community Board</h1>
            <button id="add-event" onclick="window.location.href = '/cev-form'" class="button-med-green">
                + Add an Event
            </button>
        </div> </div>

        <div id="bb-container" >

            <?php

            global $wpdb;
            global $table_name; // cev table name, was lazy in plugin

            $cevs = $wpdb->get_results("SELECT * from $table_name WHERE verified=1");


            foreach ($cevs as $cev):

            ?>
            <div id="flyer10" class="card-radius">
                    <div class="sample event-cardm">
                        <img class="pin" src="http://babblingcafe.com/wp-content/uploads/2017/11/pushpin-white-right-slant.png">
                        <div class="event-content-leftm">
                            <h2 id="event-name"><?=$cev->title?></h2>
                        </div>
                        <div class="event-content-centrem">
                            <?=$cev->description?>

                        </div>

                    </div>
            </div>

            <?php endforeach; ?>


        </div>
        </div>

        <script src="community-board.js"></script>
    </body>
</html>
