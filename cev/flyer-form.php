<!DOCTYPE html>
<html>

    <head>
        <title>Submit a Flyer for the Board</title>
        <link rel="stylesheet"  type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans">

        <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory') ?>/cev/flyer-form.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="<?php echo get_bloginfo('template_directory') ?>/cev/flyer-form.js"></script>
        <style>
         #init-buttons {
             margin-top: 24vh;
             background-color: white;
             border-radius: 25px;
             width: 60%;
             padding: 50px;
             margin-left: 15%;
         }

         #init-buttons > button{
             width: 200px;
         }
        </style>
    </head>

    <body>

        <?php


            global $wpdb;
            global $table_name;
            // Check if POST variables are defined to only execute this if the user is trying to submit something

            if (isset($_POST['title'])) {

                // For the text input we get 'title' and 'information'
                // TODO: Add form sanitation

                $title = sanitize_text_field($_POST["title"]);
                $description = stripslashes($_POST["description"]); // strip slashes for images

                $wpdb->insert(
                    $table_name,
                    array(
                        'title' => $title,
                        'description' => $description,
                        'verified' => false
                    )
                );

                echo "<p>Event added successfully.</p>";
                wp_mail(get_option('admin_email'), "A new community event has been submitted for approval", "Please review the event on the website.");
            }


        ?>


        <form id="upload-words" method="POST" style="margin-top: 4%">
            <h2 id="">Add Your Local Event Flyer</h2>
            <div class="question">
                <input type="text" name="title" required />
                <label>Event Name</label>
            </div>

            <div class="question">
                <?php wp_editor("", "description") ?>
            </div>
            <center>
                <button class="button-med-green">Submit</button>
            </center>
            </div>
        </form>



    </body>

</html>
