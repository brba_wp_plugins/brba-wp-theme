#!/bin/bash

zip -r brba-wp-theme.zip *
scp brba-wp-theme.zip 192.168.1.82:~/
ssh 192.168.1.82 "sudo -u www-data wp --path=/var/www/wordpress theme install --force ~/brba-wp-theme.zip"
rm  brba-wp-theme.zip
