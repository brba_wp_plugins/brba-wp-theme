<?php get_header() ?>

<div id="wp-content" >
    <?php

    switch ($wp->request) {

        case "":

            // Return homepage
            require_once(dirname(__FILE__) . "/home_f.php");
            break;

        case "cev-signup":
            require_once(dirname(__FILE__) . "/flyer-form.php");
            break;

        case "bulletin":
            require(dirname(__FILE__). "/cev/community-board.php");
            break;
        case "members":
            require(dirname(__FILE__). "/members/members.php");
            break;
        case "events":
            require(dirname(__FILE__). "/events/General-events.php");
            break;
        case "cev-form":
            require(dirname(__FILE__). "/cev/flyer-form.php");
            break;
        case "member-event-detail":
            require(dirname(__FILE__). "/events/specific-events.php");
            break;
        case "member-view":
            require(dirname(__FILE__). "/members/individualmember.php");
            break;
        case "member-application":
            require(dirname(__FILE__). "/members/member_form/member_form.php");
            break;
        case "email-test":
            require(dirname(__FILE__). "/email-form.php");
            break;
        case "event-rsvp":
            require(dirname(__FILE__). "/events/rsvp/RSVP-form.php");
            break;
        case "jobs":
            require(dirname(__FILE__). "/jobs/jobs.php");
            break;
        case "job-apply":
            require(dirname(__FILE__). "/jobs/apply/apply.php");
            break;



        default:
       	    if ( have_posts() ) {
                while ( have_posts() ) {
                    the_post();
                    get_template_part('content', get_post_format());
                }
            }

    }




    ?>

    <?php get_footer() ?>


    <!-- no sidebar or footer so we can ignore those -->

