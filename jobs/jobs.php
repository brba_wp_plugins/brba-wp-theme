<!DOCTYPE html>
<html>

<head>
    <title>General Events</title>
    <!-- Links stylesheets from the stylesheet, fonts, animation libraries -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/jobs/main.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory') ?>/jobs/stylesheet.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <br><br>
    <div class="grid-container" style="height: 0 !important;">
        <div class="header">
            <h1 id="title">Jobs</h1>

        </div>
        <?php

        global $wpdb;
        global $member_jobs_table;

        $jobs = $wpdb->get_results("SELECT * from $member_jobs_table");

        $i = 1;

        ?>

        <?php foreach ($jobs as $job): ?>







            <div class="events-card-<?=$i?>">
                <div class="event-card-changed">
                    <div class="events-content-left">

                    </div>
                    <div class="events-content-centre">
                        <h2 id="event-name1"><b>Name: <?=$job->job_title ?> </b></h2>
                        <p id="date-and-time1">Date and Time: <?=$job->company_name ?></p>
                        <p id="location1">Location: <?=$job->job_location ?> </p>
                        <p id="address">Address: <?=$job->street_address ?> </p>
                        <p id="website">Website: <?=$job->company_website ?> </p>
                    </div>
                    <div class="events-content-right">
                        <p id="desc1"> <?php echo $job->job_description ?> <a href="/job-apply?id=<?=$job->id?>">Apply </a> </p>
                    </div>
                </div>
            </div>


        <?php $i++; endforeach; ?>


    </div>
</body>

</html>
