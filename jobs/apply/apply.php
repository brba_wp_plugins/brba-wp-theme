<?php

global $wpdb;
global $member_job_application_table;
global $member_jobs_table;

$id = $_GET["id"];

$job = $wpdb->get_row("SELECT * FROM $member_jobs_table WHERE id=$id");


if ($_POST["form"] == "true") {

    $name = sanitize_text_field($_POST["name"]);
    $email = sanitize_text_field($_POST["email"]);
    $phone = sanitize_text_field($_POST["phone"]);
    $why = sanitize_text_field($_POST["why"]);

    $wpdb->insert(
        $member_job_application_table,
        array(
            "job_id" => $id,
            "name" => $name,
            "email" => $email,
            "phone" => $phone,
            "why" => $why
        )
    );




}


?>

<!DOCTYPE html>
<html style="background-color:#69B578;">

<head>
    <title>Apply for <?=$job->job_title?></title>
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory') ?>/jobs/apply/main.css">

    <script>
        window.console = window.console || function (t) { };
    </script>

</head>

<body translate="no">

    <div class="RSVP-page" style="margin-top: 100px;">
        <form method="POST">

            <input type="hidden" name="form" value="true">

        <center>
            <h1>Apply for <?=$job->job_title?></h1>
            <button id="return-to-events" onclick="window.location.href = '/jobs';" class="button-med-green">
                Return to Jobs
            </button>
        </center>

        <div class="question">
            <input type="text" required name="name" />
            <label>Name</label>
        </div>
        <div class="question">
            <input type="text" required name="email" />
            <label>Email Address</label>
        </div>
        <div class="question">
            <input type="text" required name="phone" />
            <label>Phone Number</label>
        </div>
        <div class="question">
            <input type="text" required name="why" />
            <label>Why do you want this job?</label>
        </div>
        <div class="RSVP-form-button">
            <button id="submit" input ="submit" class="button-med-green">
                Submit
            </button>
        </div>

        </form>


    </div>


</body>

</html>
